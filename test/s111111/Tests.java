package s111111;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class Tests {
	
	Social social;
	
	@Before
	public void setUp() throws PersonExistsException {
		social = new Social();
		social.addPerson("Solid Snake", "David", "Doe");
		social.addPerson("Liquid Snake", "Eli", "BadGuy");
		social.addPerson("Solidus Snake", "George", "Sears");
		social.addPerson("Zero", "David", "Oh");
		social.addPerson("Big Boss", "John", "WhoKnows");	
	}
	
	@Test
	public void testR1() throws NoSuchCodeException {
		assertEquals("Solid Snake David Doe", social.getPerson("Solid Snake"));
		assertEquals("Liquid Snake Eli BadGuy", social.getPerson("Liquid Snake"));
		assertEquals("Solidus Snake George Sears", social.getPerson("Solidus Snake"));
		assertEquals("Zero David Oh", social.getPerson("Zero"));
	}
	
	@Test(expected = PersonExistsException.class)
	public void testR1_AlreadyPresent() throws PersonExistsException {
		social.addPerson("Big Boss", "Venom", "Snake");
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR1_NoSuchCode() throws NoSuchCodeException {
		social.getPerson("Venom Snake");
	}
	
	@Test
	public void testR2() throws NoSuchCodeException {
		social.addFriendship("Zero", "Big Boss");
		social.addFriendship("Big Boss", "Liquid Snake");
		social.addFriendship("Big Boss", "Solidus Snake");
		social.addFriendship("Big Boss", "Solid Snake");
		
		assertTrue(social.listOfFriends("Big Boss").contains("Zero"));
		assertTrue(social.listOfFriends("Big Boss").contains("Solid Snake"));
		assertTrue(social.listOfFriends("Big Boss").contains("Liquid Snake"));
		assertTrue(social.listOfFriends("Big Boss").contains("Solidus Snake"));
		
		assertTrue(social.friendsOfFriends("Solid Snake").contains("Liquid Snake"));
		assertTrue(social.friendsOfFriends("Solid Snake").contains("Solidus Snake"));
		assertTrue(social.friendsOfFriends("Solid Snake").contains("Zero"));
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR2_NoSuchCode() throws NoSuchCodeException {
		social.addFriendship("Otacon", "Solid Snake");
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR2_NoSuchCode2() throws NoSuchCodeException {
		social.friendsOfFriends("Otacon");
	}
	
	@Test
	public void testR3() throws NoSuchCodeException {
		social.addGroup("Snakes");
		social.addGroup("The Boss Rulez");
		social.addGroup("Lookalikes");
		
		assertTrue(social.listOfGroups().contains("Snakes"));
		assertTrue(social.listOfGroups().contains("The Boss Rulez"));
		assertTrue(social.listOfGroups().contains("Lookalikes"));
		
		social.addPersonToGroup("Solid Snake", "Snakes");
		social.addPersonToGroup("Liquid Snake", "Snakes");
		social.addPersonToGroup("Solidus Snake", "Snakes");
		
		assertTrue(social.listOfPeopleInGroup("Snakes").contains("Solid Snake"));
		assertTrue(social.listOfPeopleInGroup("Snakes").contains("Liquid Snake"));
		assertTrue(social.listOfPeopleInGroup("Snakes").contains("Solidus Snake"));
		
		social.addPersonToGroup("Big Boss", "The Boss Rulez");
		social.addPersonToGroup("Zero", "The Boss Rulez");
		
		assertTrue(social.listOfPeopleInGroup("The Boss Rulez").contains("Big Boss"));
		assertTrue(social.listOfPeopleInGroup("The Boss Rulez").contains("Zero"));
		
		social.addPersonToGroup("Big Boss", "Lookalikes");
		social.addPersonToGroup("Solidus Snake", "Lookalikes");
		
		assertTrue(social.listOfPeopleInGroup("Lookalikes").contains("Big Boss"));
		assertTrue(social.listOfPeopleInGroup("Lookalikes").contains("Solidus Snake"));
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR3_NoSuchCode() throws NoSuchCodeException {
		social.addGroup("Snakes");
		social.addGroup("The Boss Rulez");
		social.addGroup("Lookalikes");
		
		social.addPersonToGroup("Vulcan Raven", "Foxhound");
	}
	
	@Test
	public void testR4() throws NoSuchCodeException {
		social.addFriendship("Zero", "Big Boss");
		social.addFriendship("Big Boss", "Liquid Snake");
		social.addFriendship("Big Boss", "Solidus Snake");
		social.addFriendship("Big Boss", "Solid Snake");
		
		social.addGroup("Snakes");
		social.addGroup("The Boss Rulez");
		social.addGroup("Lookalikes");
		social.addGroup("Literally anyone but Solidus");
		
		social.addPersonToGroup("Solid Snake", "Snakes");
		social.addPersonToGroup("Liquid Snake", "Snakes");
		social.addPersonToGroup("Solidus Snake", "Snakes");
		
		social.addPersonToGroup("Big Boss", "The Boss Rulez");
		social.addPersonToGroup("Zero", "The Boss Rulez");
		
		social.addPersonToGroup("Big Boss", "Lookalikes");
		social.addPersonToGroup("Solidus Snake", "Lookalikes");
		
		social.addPersonToGroup("Solid Snake", "Literally anyone but Solidus");
		social.addPersonToGroup("Liquid Snake", "Literally anyone but Solidus");
		social.addPersonToGroup("Zero", "Literally anyone but Solidus");
		social.addPersonToGroup("Big Boss", "Literally anyone but Solidus");
		
		assertEquals("Big Boss", social.personWithLargestNumberOfFriends());
		
		assertEquals("Zero", social.personWithMostFriendsOfFriends());
		
		assertEquals("Literally anyone but Solidus", social.largestGroup());
		
		assertEquals("Big Boss", social.personInLargestNumberOfGroups());
		
		
	}
}
